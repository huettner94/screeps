use screeps::{
    find, memory::MemoryReference, HasPosition, ReturnCode, RoomObjectProperties,
    SharedCreepProperties, Structure,
};

use crate::creepmanager::CreepManager;

use super::tasks::{Task, REPAIR_TASK_ID};

#[derive(Debug)]
pub struct RepairTask {
    target_x: u32,
    target_y: u32,
}

impl RepairTask {
    pub fn new(target_x: u32, target_y: u32) -> Self {
        Self { target_x, target_y }
    }

    pub fn load(mem: &MemoryReference) -> Option<Self> {
        let target_x = mem.get("target_x").unwrap_or_default()?;
        let target_y = mem.get("target_y").unwrap_or_default()?;
        return Some(RepairTask::new(target_x, target_y));
    }
}

impl Task for RepairTask {
    fn get_id(&self) -> u32 {
        return REPAIR_TASK_ID;
    }

    fn save(&self, mem: &MemoryReference) {
        mem.set("target_x", self.target_x);
        mem.set("target_y", self.target_y);
    }

    fn run(&self, creepmanager: &CreepManager) -> Result<bool, &'static str> {
        let structures: Vec<Structure> = creepmanager
            .creep
            .room()
            .ok_or("Unable to find room")?
            .find(find::STRUCTURES)
            .into_iter()
            .filter(|s| s.pos().x() == self.target_x && s.pos().y() == self.target_y)
            .collect();
        for structure in structures {
            // Filter out if structure really needs repair (fixes issue if there are multiple struct on one space)
            match structure.as_attackable() {
                Some(struc) => {
                    if struc.hits() >= struc.hits_max() {
                        continue;
                    }
                }
                None => continue,
            }
            let buildcode = creepmanager.creep.repair(&structure);
            match buildcode {
                ReturnCode::Ok => {
                    return Ok(false);
                }
                ReturnCode::InvalidTarget => {
                    return Ok(true);
                }
                ReturnCode::NotInRange => {
                    creepmanager.creep.move_to(&structure);
                    return Ok(false);
                }
                ReturnCode::NotEnough => {
                    return Ok(true);
                }
                _ => {
                    return Err("Cant repair for other reasons");
                }
            }
        }
        return Ok(true);
    }
}
