use log::info;
use screeps::{
    find, memory::MemoryReference, HasPosition, ResourceType, ReturnCode, RoomObjectProperties,
    SharedCreepProperties, Structure,
};

use crate::creepmanager::CreepManager;

use super::tasks::{Task, PICKUP_TASK_ID};

#[derive(Debug)]
pub struct PickupTask {
    target_x: u32,
    target_y: u32,
    resource_type: ResourceType,
}

impl PickupTask {
    pub fn new(target_x: u32, target_y: u32, resource_type: ResourceType) -> Self {
        Self {
            target_x,
            target_y,
            resource_type,
        }
    }

    pub fn load<'a>(mem: &MemoryReference) -> Option<Self> {
        let target_x = mem.get("target_x").unwrap_or_default()?;
        let target_y = mem.get("target_y").unwrap_or_default()?;
        let resource_type = mem
            .get::<ResourceType>("resource_type")
            .unwrap_or_default()?;
        return Some(PickupTask::new(target_x, target_y, resource_type));
    }
}

impl Task for PickupTask {
    fn get_id(&self) -> u32 {
        return PICKUP_TASK_ID;
    }

    fn save(&self, mem: &MemoryReference) {
        mem.set("target_x", self.target_x);
        mem.set("target_y", self.target_y);
        mem.set("resource_type", self.resource_type as u32);
    }

    fn run(&self, creepmanager: &CreepManager) -> Result<bool, &'static str> {
        let targets: Vec<Structure> = creepmanager
            .creep
            .room()
            .ok_or("Unable to find room")?
            .find(find::STRUCTURES)
            .into_iter()
            .filter(|s| {
                s.as_withdrawable().is_some()
                    && s.pos().x() == self.target_x
                    && s.pos().y() == self.target_y
            })
            .collect();
        if targets.len() == 0 {
            return Err("Target vanished during pickup!");
        }
        let target = &targets[0];
        let buildcode = creepmanager.creep.withdraw_all(
            target.as_withdrawable().expect("Cant happen"),
            self.resource_type,
        );
        match buildcode {
            ReturnCode::Ok => {
                return Ok(true);
            }
            ReturnCode::NotInRange => {
                creepmanager.creep.move_to(target);
                return Ok(false);
            }
            ReturnCode::NotEnough => {
                info!("Wanted pickup something but it is no longer there");
                return Ok(true);
            }
            _ => {
                return Err("Cant pickup for other reasons");
            }
        }
    }
}
