use screeps::{
    find, memory::MemoryReference, ConstructionSite, HasPosition, ReturnCode, RoomObjectProperties,
    SharedCreepProperties,
};

use crate::creepmanager::CreepManager;

use super::tasks::{Task, BUILD_TASK_ID};

#[derive(Debug)]
pub struct BuildTask {
    target_x: u32,
    target_y: u32,
}

impl BuildTask {
    pub fn new(target_x: u32, target_y: u32) -> Self {
        Self { target_x, target_y }
    }

    pub fn load(mem: &MemoryReference) -> Option<Self> {
        let target_x = mem.get("target_x").unwrap_or_default()?;
        let target_y = mem.get("target_y").unwrap_or_default()?;
        return Some(BuildTask::new(target_x, target_y));
    }
}

impl Task for BuildTask {
    fn get_id(&self) -> u32 {
        return BUILD_TASK_ID;
    }

    fn save(&self, mem: &MemoryReference) {
        mem.set("target_x", self.target_x);
        mem.set("target_y", self.target_y);
    }

    fn run(&self, creepmanager: &CreepManager) -> Result<bool, &'static str> {
        let structures: Vec<ConstructionSite> = creepmanager
            .creep
            .room()
            .ok_or("Unable to find room")?
            .find(find::CONSTRUCTION_SITES)
            .into_iter()
            .filter(|s| s.pos().x() == self.target_x && s.pos().y() == self.target_y)
            .collect();
        if structures.len() == 0 {
            return Ok(true);
        }
        let structure = &structures[0];
        let buildcode = creepmanager.creep.build(structure);
        match buildcode {
            ReturnCode::Ok => {
                return Ok(false);
            }
            ReturnCode::InvalidTarget => {
                return Ok(true);
            }
            ReturnCode::NotInRange => {
                creepmanager.creep.move_to(structure);
                return Ok(false);
            }
            ReturnCode::NotEnough => {
                return Ok(true);
            }
            _ => {
                return Err("Cant build for other reasons");
            }
        }
    }
}
