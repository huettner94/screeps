use log::{info, warn};
use screeps::{
    find, memory::MemoryReference, HasPosition, HasStore, ResourceType, ReturnCode,
    RoomObjectProperties, SharedCreepProperties, Structure,
};

use crate::creepmanager::CreepManager;

use super::tasks::{Task, TRANSPORT_TASK_ID};

#[derive(Debug)]
pub struct TransportTask {
    source_x: u32,
    source_y: u32,
    target_x: u32,
    target_y: u32,
    resource_type: ResourceType,
}

impl TransportTask {
    pub fn new(
        source_x: u32,
        source_y: u32,
        target_x: u32,
        target_y: u32,
        resource_type: ResourceType,
    ) -> Self {
        Self {
            source_x,
            source_y,
            target_x,
            target_y,
            resource_type,
        }
    }

    pub fn load<'a>(mem: &MemoryReference) -> Option<Self> {
        let source_x = mem.get("source_x").unwrap_or_default()?;
        let source_y = mem.get("source_y").unwrap_or_default()?;
        let target_x = mem.get("target_x").unwrap_or_default()?;
        let target_y = mem.get("target_y").unwrap_or_default()?;
        let resource_type = mem
            .get::<ResourceType>("resource_type")
            .unwrap_or_default()?;
        return Some(TransportTask::new(
            source_x,
            source_y,
            target_x,
            target_y,
            resource_type,
        ));
    }
}

impl Task for TransportTask {
    fn get_id(&self) -> u32 {
        return TRANSPORT_TASK_ID;
    }

    fn save(&self, mem: &MemoryReference) {
        mem.set("source_x", self.source_x);
        mem.set("source_y", self.source_y);
        mem.set("target_x", self.target_x);
        mem.set("target_y", self.target_y);
        mem.set("resource_type", self.resource_type as u32);
    }

    fn run(&self, creepmanager: &CreepManager) -> Result<bool, &'static str> {
        if creepmanager.creep.store_used_capacity(None)
            > creepmanager
                .creep
                .store_used_capacity(Some(self.resource_type))
        {
            match creepmanager
                .creep
                .room()
                .ok_or("Unable to find room")?
                .find(find::STRUCTURES)
                .into_iter()
                .filter_map(|s| match s {
                    Structure::Storage(s) => Some(s),
                    _ => None,
                })
                .next()
            {
                Some(storage) => {
                    for rt in creepmanager
                        .creep
                        .store_types()
                        .into_iter()
                        .filter(|r| r != &self.resource_type)
                    {
                        let buildcode = creepmanager.creep.transfer_all(&storage, rt);
                        match buildcode {
                            ReturnCode::Ok => {
                                return Ok(false);
                            }
                            ReturnCode::NotInRange => {
                                creepmanager.creep.move_to(&storage);
                                return Ok(false);
                            }
                            _ => {
                                return Err("Cant cleanup trash for other reasons");
                            }
                        }
                    }
                }
                None => warn!("We have trash but cant dump it to a storage!"),
            };
        }
        if creepmanager
            .creep
            .store_free_capacity(Some(self.resource_type))
            > 0
        {
            let sources: Vec<Structure> = creepmanager
                .creep
                .room()
                .ok_or("Unable to find room")?
                .find(find::STRUCTURES)
                .into_iter()
                .filter(|s| {
                    s.as_withdrawable().is_some()
                        && s.pos().x() == self.source_x
                        && s.pos().y() == self.source_y
                })
                .collect();
            if sources.len() == 0 {
                return Err("Source vanished during pickup!");
            }
            let source = &sources[0];
            let buildcode = creepmanager.creep.withdraw_all(
                source.as_withdrawable().expect("Cant happen"),
                self.resource_type,
            );
            match buildcode {
                ReturnCode::Ok => {
                    return Ok(false);
                }
                ReturnCode::NotInRange => {
                    creepmanager.creep.move_to(source);
                    return Ok(false);
                }
                ReturnCode::NotEnough => {
                    info!("Wanted pickup something but it is no longer there");
                    return Ok(true);
                }
                _ => {
                    return Err("Cant pickup for other reasons");
                }
            }
        } else {
            let targets: Vec<Structure> = creepmanager
                .creep
                .room()
                .ok_or("Unable to find room")?
                .find(find::STRUCTURES)
                .into_iter()
                .filter(|s| {
                    s.as_transferable().is_some()
                        && s.pos().x() == self.target_x
                        && s.pos().y() == self.target_y
                })
                .collect();
            if targets.len() == 0 {
                return Err("Target vanished during dropoff!");
            }
            let target = &targets[0];
            let buildcode = creepmanager.creep.transfer_all(
                target.as_transferable().expect("Cant happen"),
                self.resource_type,
            );
            match buildcode {
                ReturnCode::Ok => {
                    return Ok(true);
                }
                ReturnCode::NotInRange => {
                    creepmanager.creep.move_to(target);
                    return Ok(false);
                }
                ReturnCode::Full => {
                    return Ok(true);
                }
                _ => {
                    warn!("Encountered an issue during dropoff: {:?}", buildcode);
                    return Err("Cant dropoff for other reasons");
                }
            }
        }
    }
}
