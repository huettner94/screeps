use screeps::{
    find, memory::MemoryReference, HasPosition, Mineral, RoomObjectProperties,
    SharedCreepProperties, Source,
};

use crate::creepmanager::CreepManager;

use super::tasks::{Task, HARVEST_TASK_ID};

#[derive(Debug)]
pub struct HarvestTask {
    container_x: u32,
    container_y: u32,
    source_x: u32,
    source_y: u32,
}

impl HarvestTask {
    pub fn new(container_x: u32, container_y: u32, source_x: u32, source_y: u32) -> Self {
        Self {
            container_x,
            container_y,
            source_x,
            source_y,
        }
    }

    pub fn load(mem: &MemoryReference) -> Option<Self> {
        let container_x = mem.get("container_x").unwrap_or_default()?;
        let container_y = mem.get("container_y").unwrap_or_default()?;
        let source_x = mem.get("source_x").unwrap_or_default()?;
        let source_y = mem.get("source_y").unwrap_or_default()?;
        return Some(HarvestTask::new(
            container_x,
            container_y,
            source_x,
            source_y,
        ));
    }
}

impl Task for HarvestTask {
    fn get_id(&self) -> u32 {
        return HARVEST_TASK_ID;
    }

    fn save(&self, mem: &MemoryReference) {
        mem.set("container_x", self.container_x);
        mem.set("container_y", self.container_y);
        mem.set("source_x", self.source_x);
        mem.set("source_y", self.source_y);
    }

    fn run(&self, creepmanager: &CreepManager) -> Result<bool, &'static str> {
        let pos = creepmanager.creep.pos();
        if pos.x() != self.container_x || pos.y() != self.container_y {
            creepmanager
                .creep
                .move_to_xy(self.container_x, self.container_y);
        } else {
            let sources: Vec<Source> = creepmanager
                .creep
                .room()
                .ok_or("Unable to find room")?
                .find(find::SOURCES)
                .into_iter()
                .filter(|s| s.pos().x() == self.source_x && s.pos().y() == self.source_y)
                .collect();
            if sources.len() == 1 {
                creepmanager.creep.harvest(&sources[0]);
            } else {
                let minerals: Vec<Mineral> = creepmanager
                    .creep
                    .room()
                    .ok_or("Unable to find room")?
                    .find(find::MINERALS)
                    .into_iter()
                    .filter(|s| s.pos().x() == self.source_x && s.pos().y() == self.source_y)
                    .collect();
                if minerals.len() == 1 {
                    creepmanager.creep.harvest(&minerals[0]);
                } else {
                    return Err("Could not find source or mineral to harvest");
                }
            }
        }
        return Ok(false);
    }
}
