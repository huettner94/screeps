use std::fmt::Debug;

use log::warn;
use screeps::memory::MemoryReference;

use crate::creepmanager::CreepManager;

use super::buildtask::BuildTask;
use super::dropofftask::DropoffTask;
use super::harvestonecetask::HarvestOnceTask;
use super::harvesttask::HarvestTask;
use super::pickuptask::PickupTask;
use super::repairtask::RepairTask;
use super::transporttask::TransportTask;
use super::upgradetask::UpgradeTask;

pub const HARVEST_TASK_ID: u32 = 1;
pub const REPAIR_TASK_ID: u32 = 2;
pub const BUILD_TASK_ID: u32 = 3;
pub const PICKUP_TASK_ID: u32 = 4;
pub const UPGRADE_TASK_ID: u32 = 5;
pub const DROPOFF_TASK_ID: u32 = 6;
pub const TRANSPORT_TASK_ID: u32 = 7;
pub const HARVEST_ONCE_TASK_ID: u32 = 8;

pub trait Task: Debug {
    fn get_id(&self) -> u32;
    fn save(&self, mem: &MemoryReference);
    fn run(&self, creepmanager: &CreepManager) -> Result<bool, &'static str>;
}

pub fn get_task(mem: &MemoryReference) -> Option<Box<dyn Task>> {
    match mem.get("task_id").unwrap_or_default() {
        None => {
            return None;
        }
        Some(task_id) => match task_id {
            HARVEST_TASK_ID => {
                return Some(Box::new(HarvestTask::load(&mem)?));
            }
            REPAIR_TASK_ID => {
                return Some(Box::new(RepairTask::load(&mem)?));
            }
            BUILD_TASK_ID => {
                return Some(Box::new(BuildTask::load(&mem)?));
            }
            PICKUP_TASK_ID => {
                return Some(Box::new(PickupTask::load(&mem)?));
            }
            UPGRADE_TASK_ID => {
                return Some(Box::new(UpgradeTask::load(&mem)?));
            }
            DROPOFF_TASK_ID => {
                return Some(Box::new(DropoffTask::load(&mem)?));
            }
            TRANSPORT_TASK_ID => {
                return Some(Box::new(TransportTask::load(&mem)?));
            }
            HARVEST_ONCE_TASK_ID => {
                return Some(Box::new(HarvestOnceTask::load(&mem)?));
            }
            _ => {
                warn!("Task id is unkown");
                return None;
            }
        },
    }
}
