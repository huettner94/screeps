use screeps::{
    memory::MemoryReference, HasPosition, ReturnCode, RoomObjectProperties, SharedCreepProperties,
};

use crate::creepmanager::CreepManager;

use super::tasks::{Task, UPGRADE_TASK_ID};

#[derive(Debug)]
pub struct UpgradeTask {
    target_x: u32,
    target_y: u32,
}

impl UpgradeTask {
    pub fn new(target_x: u32, target_y: u32) -> Self {
        Self { target_x, target_y }
    }

    pub fn load(mem: &MemoryReference) -> Option<Self> {
        let target_x = mem.get("target_x").unwrap_or_default()?;
        let target_y = mem.get("target_y").unwrap_or_default()?;
        return Some(UpgradeTask::new(target_x, target_y));
    }
}

impl Task for UpgradeTask {
    fn get_id(&self) -> u32 {
        return UPGRADE_TASK_ID;
    }

    fn save(&self, mem: &MemoryReference) {
        mem.set("target_x", self.target_x);
        mem.set("target_y", self.target_y);
    }

    fn run(&self, creepmanager: &CreepManager) -> Result<bool, &'static str> {
        match creepmanager
            .creep
            .room()
            .ok_or("Unable to find room")?
            .controller()
        {
            Some(controller) => {
                if controller.pos().x() != self.target_x || controller.pos().y() != self.target_y {
                    return Err("Controller is at wrong position. WTF!");
                }
                let buildcode = creepmanager.creep.upgrade_controller(&controller);
                match buildcode {
                    ReturnCode::Ok => {
                        return Ok(false);
                    }
                    ReturnCode::NotInRange => {
                        creepmanager.creep.move_to(&controller);
                        return Ok(false);
                    }
                    ReturnCode::NotEnough => {
                        return Ok(true);
                    }
                    _ => {
                        return Err("Cant upgrade for other reasons");
                    }
                }
            }
            None => {
                return Err("Could not find controller for room!");
            }
        }
    }
}
