use log::warn;
use screeps::{
    find, memory::MemoryReference, HasPosition, HasStore, ResourceType, ReturnCode,
    RoomObjectProperties, SharedCreepProperties, Source,
};

use crate::creepmanager::CreepManager;

use super::tasks::{Task, HARVEST_ONCE_TASK_ID};

#[derive(Debug)]
pub struct HarvestOnceTask {
    source_x: u32,
    source_y: u32,
}

impl HarvestOnceTask {
    pub fn new(source_x: u32, source_y: u32) -> Self {
        Self { source_x, source_y }
    }

    pub fn load(mem: &MemoryReference) -> Option<Self> {
        let source_x = mem.get("source_x").unwrap_or_default()?;
        let source_y = mem.get("source_y").unwrap_or_default()?;
        return Some(HarvestOnceTask::new(source_x, source_y));
    }
}

impl Task for HarvestOnceTask {
    fn get_id(&self) -> u32 {
        return HARVEST_ONCE_TASK_ID;
    }

    fn save(&self, mem: &MemoryReference) {
        mem.set("source_x", self.source_x);
        mem.set("source_y", self.source_y);
    }

    fn run(&self, creepmanager: &CreepManager) -> Result<bool, &'static str> {
        if creepmanager
            .creep
            .store_free_capacity(Some(ResourceType::Energy))
            == 0
        {
            return Ok(true);
        }
        let targets: Vec<Source> = creepmanager
            .creep
            .room()
            .ok_or("Unable to find room")?
            .find(find::SOURCES)
            .into_iter()
            .filter(|s| s.pos().x() == self.source_x && s.pos().y() == self.source_y)
            .collect();
        if targets.len() == 0 {
            warn!("Source vanished during mining!");
            return Ok(true);
        }
        let target = &targets[0];
        let buildcode = creepmanager.creep.harvest(target);
        match buildcode {
            ReturnCode::Ok => {
                return Ok(false);
            }
            ReturnCode::NotInRange => {
                creepmanager.creep.move_to(target);
                return Ok(false);
            }
            _ => {
                return Err("Cant dropoff for other reasons");
            }
        }
    }
}
