#![deny(clippy::unwrap_used)]
use std::collections::HashSet;

use log::*;
use screeps::RoomObjectProperties;
use stdweb::js;

mod creepmanager;
mod logging;
mod roommanager;
mod tasks;
use roommanager::RoomManager;

use crate::creepmanager::CreepManager;

fn main() {
    logging::setup_logging(logging::Info);

    js! {
        var game_loop = @{game_loop};

        module.exports.loop = function() {
            // Provide actual error traces.
            try {
                game_loop();
            } catch (error) {
                // console_error function provided by 'screeps-game-api'
                console_error("caught exception:", error);
                if (error.stack) {
                    console_error("stack trace:", error.stack);
                }
                console_error("resetting VM next tick.");
                // reset the VM since we don't know if everything was cleaned up and don't
                // want an inconsistent state.
                module.exports.loop = wasm_initialize;
            }
        }
    }
}

fn game_loop() {
    debug!("loop starting! CPU: {}", screeps::game::cpu::get_used());

    let rooms: Vec<RoomManager> = screeps::game::spawns::values()
        .into_iter()
        .filter_map(|s| s.room())
        .map(|room| RoomManager::new(room))
        .collect();

    let creeps: Vec<CreepManager> = screeps::game::creeps::values()
        .into_iter()
        .map(|c| CreepManager::new(c))
        .collect();

    for room in rooms {
        room.manage_room(&creeps);
    }

    for creep in &creeps {
        creep.manage_creep();
    }

    let time = screeps::game::time();

    if time % 32 == 3 {
        info!("running memory cleanup");
        match cleanup_memory() {
            Ok(_) => {}
            Err(e) => {
                warn!("error duing memory cleanup: {}", e);
            }
        }
    }

    info!("done! cpu: {}", screeps::game::cpu::get_used())
}

fn cleanup_memory() -> Result<(), Box<dyn std::error::Error>> {
    let alive_creeps: HashSet<String> = screeps::game::creeps::keys().into_iter().collect();

    let screeps_memory = match screeps::memory::root().dict("creeps")? {
        Some(v) => v,
        None => {
            warn!("not cleaning game creep memory: no Memory.creeps dict");
            return Ok(());
        }
    };

    for mem_name in screeps_memory.keys() {
        if !alive_creeps.contains(&mem_name) {
            debug!("cleaning up creep memory of dead creep {}", mem_name);
            screeps_memory.del(&mem_name);
        }
    }

    Ok(())
}
