use log::{info, warn};
use screeps::{Creep, SharedCreepProperties};

use crate::tasks::tasks::Task;

pub struct CreepManager {
    pub creep: Creep,
}

impl CreepManager {
    pub fn new(creep: Creep) -> Self {
        Self { creep }
    }

    pub fn set_task(&self, task: &impl Task) {
        match self.creep.memory().dict_or_create("task_info") {
            Err(_) => warn!("Error setting task on creep"),
            Ok(mem) => {
                task.save(&mem);
                mem.set("task_id", task.get_id());
            }
        }
    }

    pub fn get_task(&self) -> Option<Box<dyn Task>> {
        let task_info = self.creep.memory().dict_or_create("task_info").ok()?;
        return crate::tasks::tasks::get_task(&task_info);
    }

    pub fn manage_creep(&self) {
        if self.creep.spawning() {
            return;
        }
        match self.get_task() {
            None => info!("No task for creep {}", self.creep.name()),
            Some(task) => match task.run(&self) {
                Ok(ret) => {
                    if ret {
                        self.creep.memory().del("task_info");
                        info!("Creep {} finished task", self.creep.name());
                    }
                }
                Err(err) => {
                    self.creep.memory().del("task_info");
                    warn!(
                        "Task of {} had issue during run: {}; {:?}",
                        self.creep.name(),
                        err,
                        task,
                    );
                }
            },
        }
    }

    pub fn has_task(&self) -> bool {
        match self.get_task() {
            Some(_) => return true,
            None => return false,
        }
    }
}
