use std::cmp::{max, min};
use std::collections::HashSet;
use std::iter::FromIterator;
use std::usize;

use log::*;
use screeps::game::market::{Order, OrderType};
use screeps::objects::StructureProperties;

use screeps::{
    find, ConstructionSite, HasPosition, HasStore, MarketResourceType, Part, Position,
    ResourceType, ReturnCode, Room, RoomObjectProperties, SharedCreepProperties, Structure,
    StructureTower, StructureType,
};
use screeps::{HasCooldown, StructureSpawn};
use screeps::{Source, StructureTerminal};

use crate::creepmanager::CreepManager;
use crate::tasks::buildtask::BuildTask;
use crate::tasks::harvestonecetask::HarvestOnceTask;
use crate::tasks::harvesttask::HarvestTask;
use crate::tasks::pickuptask::PickupTask;
use crate::tasks::repairtask::RepairTask;
use crate::tasks::tasks::UPGRADE_TASK_ID;
use crate::tasks::transporttask::TransportTask;
use crate::tasks::upgradetask::UpgradeTask;

const BUILDERS_AMOUNT: usize = 6;
const ENERGY_CARRIERS_AMOUNT: usize = 5;

pub struct RoomManager {
    room: Room,
    structures: Vec<Structure>,
    terminal: Option<StructureTerminal>,
}

impl<'a> RoomManager {
    pub fn new(room: Room) -> Self {
        let structures: Vec<Structure> = room.find(find::STRUCTURES);
        let terminal = room
            .find(find::STRUCTURES)
            .into_iter()
            .find_map(|s| match s {
                Structure::Terminal(t) => Some(t),
                _ => None,
            });
        Self {
            room,
            structures,
            terminal,
        }
    }

    pub fn manage_room(&self, creeps: &Vec<CreepManager>) {
        debug!("Managing room {}", self.room.name());
        self.attack_enemies();

        self.assign_harvesters();

        self.assign_builders(&creeps);

        self.assign_carriers(&creeps);

        if let Some(t) = &self.terminal {
            self.sell_stuff(t);
        }
    }

    fn attack_enemies(&self) {
        let towers: Vec<&StructureTower> = self
            .structures
            .iter()
            .filter_map(|s| match s {
                Structure::Tower(t) => Some(t),
                _ => None,
            })
            .collect();
        for tower in towers {
            match tower.pos().find_closest_by_range(find::HOSTILE_CREEPS) {
                Some(e) => {
                    tower.attack(&e);
                }
                None => {}
            }
        }
    }

    fn assign_harvesters(&self) {
        let containers = self.structures.iter().filter(|s| {
            s.structure_type() == StructureType::Container // This must be earily to make this not horribly inefficient
                && (s.pos().find_in_range(find::SOURCES, 1).len() > 0
                    || s.pos().find_in_range(find::MINERALS, 1).len() > 0)
        });
        for container in containers {
            let pos = container.pos();
            debug!("Found container at {}", pos);
            let memory_path = format!("source_{}_{}", pos.x(), pos.y());
            match self
                .room
                .memory()
                .dict(&memory_path)
                .expect("Type error when getting source")
            {
                Some(m) => {
                    let claimed: String = m
                        .get("claimed")
                        .expect("Type error when getting claimed state")
                        .unwrap_or_default();
                    if claimed.is_empty() {
                        info!("Resource is not claimed. Spawning creep");
                        let source_x = m
                            .get("source_x")
                            .expect("Type error when getting source_x")
                            .unwrap_or_default();
                        let source_y = m
                            .get("source_y")
                            .expect("Type error when getting source_x")
                            .unwrap_or_default();
                        match self.spawn_harvester(pos, source_x, source_y) {
                            None => info!("Failed to spawn creep for harvesting"),
                            Some(creep) => m.set("claimed", creep.creep.name()),
                        }
                    } else {
                        if screeps::game::creeps::get(&claimed).is_none() {
                            info!("Creep of node seems to have died, unclaiming!");
                            m.set("claimed", "");
                        }
                    }
                }
                None => {
                    let d = self
                        .room
                        .memory()
                        .dict_or_create(&memory_path)
                        .expect("error when creating resource memory");
                    let sources = pos.find_in_range(find::SOURCES, 1);
                    let resource_pos;
                    if sources.len() > 0 {
                        let source = sources.first().expect("Could not find source");
                        resource_pos = source.pos();
                    } else {
                        let minerals = pos.find_in_range(find::MINERALS, 1);
                        let mineral = minerals.first().expect("Could not find mineral");
                        resource_pos = mineral.pos();
                    }

                    d.set("claimed", "");
                    d.set("source_x", resource_pos.x());
                    d.set("source_y", resource_pos.y());
                }
            }
        }
    }

    fn spawn_harvester(&self, pos: Position, source_x: u32, source_y: u32) -> Option<CreepManager> {
        match self.spawn_creep("harvester", &[Part::Move], &[Part::Work], 5) {
            None => return None,
            Some(creep) => {
                creep.set_task(&HarvestTask::new(pos.x(), pos.y(), source_x, source_y));
                return Some(creep);
            }
        }
    }

    fn assign_builders(&self, creeps: &Vec<CreepManager>) {
        let free_creeps = self.ensure_count_and_get_free(
            creeps,
            "builder",
            &[],
            &[Part::Move, Part::Move, Part::Carry, Part::Work],
            2,
            BUILDERS_AMOUNT,
        );
        if free_creeps.len() == 0 {
            return;
        }

        let mut free_creeps_with_energy: Vec<&CreepManager> = free_creeps
            .iter()
            .filter(|c| c.creep.energy() > 0)
            .map(|c| *c)
            .collect();
        let free_creeps_without_energy: Vec<&CreepManager> = free_creeps
            .iter()
            .filter(|c| c.creep.energy() == 0)
            .map(|c| *c)
            .collect();
        let creeps_upgrading_controller = creeps
            .into_iter()
            .filter(|c| match c.get_task() {
                Some(t) => return t.get_id() == UPGRADE_TASK_ID,
                None => return false,
            })
            .count();

        let construction_sites: Vec<ConstructionSite> = self.room.find(find::MY_CONSTRUCTION_SITES);

        let requiring_repair: Vec<&Structure> = self
            .structures
            .iter()
            .filter(|s| s.as_attackable().is_some())
            .filter(|s| {
                let mut hits_max = s.as_attackable().expect("cant happen").hits_max();
                if s.structure_type() == StructureType::Wall
                    || s.structure_type() == StructureType::Rampart
                {
                    hits_max = 500000;
                }
                s.as_attackable().expect("cant happen").hits() < ((hits_max / 5) * 4)
            })
            .collect();
        let urgent_required_repair: Vec<&Structure> = requiring_repair
            .iter()
            .filter(|s| {
                let mut hits_max = s.as_attackable().expect("cant happen").hits_max();
                if s.structure_type() == StructureType::Wall
                    || s.structure_type() == StructureType::Rampart
                {
                    hits_max = 50000;
                }
                s.as_attackable().expect("cant happen").hits() < ((hits_max / 5) * 2)
            })
            .map(|s| *s)
            .collect();

        for creep in free_creeps_without_energy {
            self.fetch_energy(creep);
        }
        if creeps_upgrading_controller < 1 {
            match self.room.controller() {
                Some(controller) => match free_creeps_with_energy.pop() {
                    Some(creep) => {
                        creep.set_task(&UpgradeTask::new(
                            controller.pos().x(),
                            controller.pos().y(),
                        ));
                    }
                    None => {
                        info!("Not enough free creeps for controller upgrades");
                        return;
                    }
                },
                None => {}
            }
        }
        for structure in urgent_required_repair {
            match free_creeps_with_energy.pop() {
                Some(creep) => {
                    creep.set_task(&RepairTask::new(structure.pos().x(), structure.pos().y()));
                }
                None => {
                    info!("Not enough free creeps for all urgent repairs");
                    return;
                }
            }
        }
        for structure in construction_sites {
            match free_creeps_with_energy.pop() {
                Some(creep) => {
                    creep.set_task(&BuildTask::new(structure.pos().x(), structure.pos().y()));
                }
                None => {
                    return;
                }
            }
        }
        for structure in requiring_repair {
            match free_creeps_with_energy.pop() {
                Some(creep) => {
                    creep.set_task(&RepairTask::new(structure.pos().x(), structure.pos().y()));
                }
                None => {
                    return;
                }
            }
        }
        match self.room.controller() {
            Some(controller) => {
                for creep in free_creeps_with_energy {
                    creep.set_task(&UpgradeTask::new(
                        controller.pos().x(),
                        controller.pos().y(),
                    ));
                }
            }
            None => {}
        }
    }

    fn assign_carriers(&self, creeps: &Vec<CreepManager>) {
        let mut free_creeps = self.ensure_count_and_get_free(
            creeps,
            "energy_carrier",
            &[],
            &[Part::Move, Part::Carry],
            3,
            ENERGY_CARRIERS_AMOUNT,
        );
        if free_creeps.len() == 0 {
            return;
        }

        let structures: Vec<&Structure> = self
            .structures
            .iter()
            .filter(|s| match s.as_has_store() {
                Some(store) => {
                    if store.store_free_capacity(Some(ResourceType::Energy)) > 0 {
                        return true;
                    }
                    return false;
                }
                None => return false,
            })
            .collect();

        for tower in structures
            .iter()
            .filter(|s| s.structure_type() == StructureType::Tower)
        {
            match free_creeps.pop() {
                Some(creep) => {
                    self.assign_transport(creep, tower.pos(), ResourceType::Energy);
                }
                None => {
                    return;
                }
            }
        }
        for extension in structures
            .iter()
            .filter(|s| s.structure_type() == StructureType::Extension)
        {
            match free_creeps.pop() {
                Some(creep) => {
                    self.assign_transport(creep, extension.pos(), ResourceType::Energy);
                }
                None => {
                    return;
                }
            }
        }
        for spawn in structures
            .iter()
            .filter(|s| s.structure_type() == StructureType::Spawn)
        {
            match free_creeps.pop() {
                Some(creep) => {
                    self.assign_transport(creep, spawn.pos(), ResourceType::Energy);
                }
                None => {
                    return;
                }
            }
        }
        for lab in structures
            .iter()
            .filter(|s| s.structure_type() == StructureType::Lab)
        {
            match free_creeps.pop() {
                Some(creep) => {
                    self.assign_transport(creep, lab.pos(), ResourceType::Energy);
                }
                None => {
                    return;
                }
            }
        }

        // Get stuff to the Storage
        match self
            .structures
            .iter()
            .find_map(|s| match s {
                Structure::Storage(storage) => Some(storage),
                _ => None,
            })
            .take()
        {
            Some(storage) => {
                if storage.store_used_capacity(Some(ResourceType::Energy)) < 50000 {
                    match free_creeps.pop() {
                        Some(creep) => {
                            self.assign_transport_with_filter(
                                creep,
                                storage.pos(),
                                ResourceType::Energy,
                                Some(|s: &Structure| s.structure_type() != StructureType::Storage),
                            );
                        }
                        None => {
                            return;
                        }
                    }
                }
            }
            None => {}
        }

        // Get stuff to the Terminal
        let resource_types: HashSet<ResourceType> = HashSet::from_iter(
            self.structures
                .iter()
                .filter_map(|s| match s {
                    Structure::Container(c) => Some(c),
                    _ => None,
                })
                .flat_map(|s| s.store_types()),
        );

        if let Some(t) = &self.terminal {
            for resource_type in resource_types {
                if t.store_used_capacity(Some(resource_type)) < 2000 {
                    match free_creeps.pop() {
                        Some(creep) => {
                            if !self.assign_transport(creep, t.pos(), resource_type) {
                                free_creeps.push(creep);
                                continue;
                            }
                        }
                        None => {
                            return;
                        }
                    }
                }
            }
        }
    }

    fn sell_stuff(&self, terminal: &StructureTerminal) {
        if terminal.cooldown() > 0 {
            return;
        }
        for resource_type in terminal.store_types().into_iter().filter(|r| {
            !matches!(r, ResourceType::Energy) && terminal.store_used_capacity(Some(*r)) > 100
        }) {
            let amount = terminal.store_used_capacity(Some(resource_type));
            let mut orders: Vec<Order> = screeps::game::market::get_all_orders(Some(
                MarketResourceType::Resource(resource_type),
            ))
            .into_iter()
            .filter(|o| {
                o.order_type == OrderType::Buy
                    && o.room_name.is_some()
                    && screeps::game::market::calc_transaction_cost(
                        1000,
                        self.room.name(),
                        o.room_name.expect("Cant happen"),
                    ) < 500.0
            })
            .collect();
            if orders.len() > 0 {
                orders.sort_unstable_by(|a, b| {
                    a.price
                        .partial_cmp(&b.price)
                        .unwrap_or(std::cmp::Ordering::Equal)
                });
                let order = orders.last().expect("Cant happen");
                let real_amount = min(amount, order.remaining_amount);
                info!(
                    "Selling {} of {} for {} Money",
                    real_amount,
                    resource_type,
                    real_amount as f64 * order.price
                );
                screeps::game::market::deal(&order.id, real_amount, Some(self.room.name()));
                return;
            }
        }
    }

    fn spawn_creep(
        &self,
        suffix: &str,
        base_parts: &[Part],
        repeat_parts: &[Part],
        max_repeats: u32,
    ) -> Option<CreepManager> {
        let mut parts = base_parts.to_vec();

        if !repeat_parts.is_empty() {
            let base_cost: u32 = base_parts.into_iter().map(|p| p.cost()).sum();
            let repeat_cost: u32 = repeat_parts.into_iter().map(|p| p.cost()).sum();
            let mut max_cost = self.room.energy_available();

            max_cost -= base_cost;
            let repeats = min(max_cost / repeat_cost, max_repeats); // Division on integers always rounds down
            if repeats == 0 {
                return None; // Dont spawn a creep if we cant do any repeat
            }
            for _ in 0..repeats {
                parts.extend(repeat_parts);
            }
        }

        let name_base = screeps::game::time();
        let name = format!("{}-{}", name_base, suffix);
        match self.get_spawn()?.spawn_creep(&parts, &name) {
            ReturnCode::Ok => match screeps::game::creeps::get(&name) {
                None => {
                    warn!("Spawned creep but could not get it!");
                    return None;
                }
                Some(creep) => return Some(CreepManager::new(creep)),
            },
            _ => {
                return None;
            }
        }
    }

    fn ensure_creep_count(
        &self,
        creeps: &Vec<CreepManager>,
        suffix: &str,
        parts: &[Part],
        repeat_parts: &[Part],
        max_repeats: u32,
        amount: usize,
    ) -> Option<CreepManager> {
        let count: usize = creeps
            .iter()
            .filter(|s| s.creep.name().contains(suffix))
            .count();
        if count < amount {
            return self.spawn_creep(suffix, &parts, &repeat_parts, max_repeats);
        }
        return None;
    }

    fn ensure_count_and_get_free<'b>(
        &self,
        creeps: &'b Vec<CreepManager>,
        suffix: &'b str,
        parts: &'b [Part],
        repeat_parts: &'b [Part],
        max_repeats: u32,
        amount: usize,
    ) -> Vec<&'b CreepManager> {
        self.ensure_creep_count(creeps, suffix, parts, repeat_parts, max_repeats, amount);
        return creeps
            .into_iter()
            .filter(|c| {
                c.creep.name().contains(suffix)
                    && !c.has_task()
                    && c.creep
                        .room()
                        .expect("could not find room of creep")
                        .eq(&self.room)
            })
            .collect();
    }

    fn fetch_energy(&self, creep: &CreepManager) {
        match self.find_storage_for(creep.creep.pos(), ResourceType::Energy, 0) {
            Some(s) => {
                creep.set_task(&PickupTask::new(
                    s.pos().x(),
                    s.pos().y(),
                    ResourceType::Energy,
                ));
            }
            None => {
                if creep.creep.get_active_bodyparts(Part::Work) > 0 {
                    info!("Could not find storage to get energy from. Mining manually");
                    let mut sources: Vec<Source> = self.room.find(find::SOURCES);
                    sources.sort_unstable_by_key(|s| s.pos().get_range_to(&creep.creep.pos()));
                    if sources.len() == 0 {
                        warn!("There is no source in this room to get energy from!");
                        return;
                    }
                    let source = &sources[0];
                    creep.set_task(&HarvestOnceTask::new(source.pos().x(), source.pos().y()));
                } else {
                    info!("Could not find storage to get energy from. Needing to wait for someone to mine");
                }
            }
        }
    }

    fn assign_transport(
        &self,
        creep: &CreepManager,
        target: Position,
        resource_type: ResourceType,
    ) -> bool {
        return self.assign_transport_with_filter(
            creep,
            target,
            resource_type,
            None::<fn(&Structure) -> bool>,
        );
    }

    fn assign_transport_with_filter<P>(
        &self,
        creep: &CreepManager,
        target: Position,
        resource_type: ResourceType,
        filter: Option<P>,
    ) -> bool
    where
        P: Fn(&Structure) -> bool,
        P: Copy,
    {
        match self.find_storage_for_with_filter(
            creep.creep.pos(),
            resource_type,
            creep.creep.store_capacity(None),
            filter,
        ) {
            Some(s) => {
                creep.set_task(&TransportTask::new(
                    s.pos().x(),
                    s.pos().y(),
                    target.x(),
                    target.y(),
                    resource_type,
                ));
                return true;
            }
            None => {
                return false;
            }
        }
    }

    fn find_storage_for(
        &self,
        pos: Position,
        resource_type: ResourceType,
        amount: u32,
    ) -> Option<&Structure> {
        return self.find_storage_for_with_filter(
            pos,
            resource_type,
            amount,
            None::<fn(&Structure) -> bool>,
        );
    }

    fn find_storage_for_with_filter<P>(
        &self,
        pos: Position,
        resource_type: ResourceType,
        amount: u32,
        filter: Option<P>,
    ) -> Option<&Structure>
    where
        P: Fn(&Structure) -> bool,
        P: Copy,
    {
        let mut storages: Vec<&Structure> = self
            .structures
            .iter()
            .filter(|s| {
                match filter {
                    Some(func) => {
                        if !func(s) {
                            return false;
                        }
                    }
                    None => {}
                };
                return (s.structure_type() == StructureType::Container
                    || s.structure_type() == StructureType::Storage)
                    && s.as_has_store()
                        .expect("Cant happen")
                        .store_used_capacity(Some(resource_type))
                        >= max(amount, 1);
            })
            .collect();
        if storages.len() == 0 {
            return None;
        }
        storages.sort_unstable_by_key(|s| s.pos().get_range_to(&pos));
        return Some(&storages[0]);
    }

    fn get_spawn(&self) -> Option<StructureSpawn> {
        let mut spawns = self.room.find(find::MY_SPAWNS);
        return spawns.drain(0..1).next();
    }
}
